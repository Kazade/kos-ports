#include "../SDL_sysvideo.h"

#if SDL_VIDEO_DRIVER_DREAMCAST

#include <kos.h>
#include <GL/gl.h>
#include <GL/glut.h>

typedef struct KGLContext {

} KGLContext;

int Dreamcast_GL_LoadLibrary(_THIS, const char *path)
{
    _this->gl_config.driver_loaded = 1;
    return 0;
}

void Dreamcast_GL_UnloadLibrary(_THIS)
{
    _this->gl_config.driver_loaded = 0;
    return;
}

void* Dreamcast_GL_GetProcAddress(_THIS, const char *proc)
{
    if(strcmp(proc, "glBegin") == 0) {
        return &glBegin;
    } else if(strcmp(proc, "glEnd") == 0) {
        return &glEnd;
    } else if(strcmp(proc, "glGetString") == 0) {
        return &glGetString;
    }

    return NULL;
}

SDL_GLContext Dreamcast_GL_CreateContext(_THIS, SDL_Window * window)
{
    KGLContext* context = (KGLContext*) malloc(sizeof(KGLContext));
    glKosInit();
    return context;
}

int Dreamcast_GL_MakeCurrent(_THIS, SDL_Window * window, SDL_GLContext context)
{
    return 0;
}

int Dreamcast_GL_SetSwapInterval(_THIS, int interval)
{
    return SDL_Unsupported();
}

int Dreamcast_GL_GetSwapInterval(_THIS)
{
    return SDL_Unsupported();
}

void Dreamcast_GL_SwapWindow(_THIS, SDL_Window * window)
{
    glutSwapBuffers();
}

void Dreamcast_GL_DeleteContext(_THIS, SDL_GLContext context)
{
    free(context);
    return;
}

#endif

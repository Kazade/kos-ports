#include "../../SDL_internal.h"

#ifdef SDL_TIMER_DREAMCAST

#include <kos.h>
#include "SDL_timer.h"

static unsigned start;

void
SDL_TicksInit(void)
{
    /* Set first ticks value */
    start = timer_ms_gettime64();
}

void
SDL_TicksQuit(void)
{
    return;
}

Uint32
SDL_GetTicks(void)
{
    return(timer_ms_gettime64() - start);
}

Uint64
SDL_GetPerformanceCounter(void)
{
    return SDL_GetTicks();
}

Uint64
SDL_GetPerformanceFrequency(void)
{
    return 1000;
}

void
SDL_Delay(Uint32 ms)
{
    thd_sleep(ms);
}


#endif

#ifndef SDL_DREAMCASTEVENTS_C_H
#define SDL_DREAMCASTEVENTS_C_H

#include "SDL_dreamcastvideo.h"

extern void Dreamcast_InitOSKeymap(_THIS);
extern void Dreamcast_PumpEvents(_THIS);

#endif

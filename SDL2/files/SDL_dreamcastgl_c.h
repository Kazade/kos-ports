
#ifndef SDL_DREAMCASTGL_C_H
#define SDL_DREAMCASTGL_C_H

extern int Dreamcast_GL_LoadLibrary(_THIS, const char *path);
extern void* Dreamcast_GL_GetProcAddress(_THIS, const char *proc);
extern void Dreamcast_GL_UnloadLibrary(_THIS);
extern SDL_GLContext Dreamcast_GL_CreateContext(_THIS, SDL_Window * window);
extern int Dreamcast_GL_MakeCurrent(_THIS, SDL_Window * window, SDL_GLContext context);
extern int Dreamcast_GL_SetSwapInterval(_THIS, int interval);
extern int Dreamcast_GL_GetSwapInterval(_THIS);
extern void Dreamcast_GL_SwapWindow(_THIS, SDL_Window * window);
extern void Dreamcast_GL_DeleteContext(_THIS, SDL_GLContext context);

#endif
